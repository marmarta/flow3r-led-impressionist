from st3m.reactor import Responder
from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.input import InputController, InputState, TouchableState
import st3m.run
import leds
import math

class LedImpression(Application):
    def __init__(self, app_ctx) -> None:
        super().__init__(app_ctx)
        self.text = ""
        self.input = InputController()
        self.leds = [] # should be a list of four-item tuples, led, h, s, v (or r, g, b?)
        self.sat = 1
        self.int_mod = 10

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.move_to(0,0)
        ctx.font = ctx.get_font_name(4)
        ctx.font_size = 20
        ctx.move_to(-30, 0).rgb(255, 255, 255).text(self.text)
        for (led, h, s, v) in self.leds:
            leds.set_hsv(led, h, s, v)
        leds.update()
            #leds.set_hsv(led, self.h, self.s, self.v)

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)
        self.leds = []
        saturation_petal = self.input.captouch.petals[1]
        if saturation_petal.gesture.current_gesture():
            self.sat = min(abs(saturation_petal.gesture.current_gesture().velocity[0]/100000),1)
        intensity_petal = self.input.captouch.petals[9]
        if intensity_petal.gesture.current_gesture():
            self.int_mod = min(abs(intensity_petal.gesture.current_gesture().velocity[0]/10000),10)
        for i in [0,2,4,6,8]:
            if ins.captouch.petals[i].pressed:
                # 0 - 0-8
                for j in range(0, 8):
                    d, a = ins.captouch.petals[i].position
                    hue = abs(math.sin(d/1000) * 360)
                    self.leds.append(((int(4*i)+j-4)%40, (hue + self.int_mod*j)%360, self.sat, 1))

if __name__ == '__main__':
    st3m.run.run_view(LedImpression(ApplicationContext()))
